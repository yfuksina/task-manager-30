package ru.tsc.fuksina.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.model.User;

public interface IAuthService {

    void login(@Nullable String login, @Nullable String password);

    void logout();

    @Nullable
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    User getUser();

    String getUserId();

    boolean isAuth();

    void checkRoles(Role[] roles);
}
